# Q900-Display

#### 介绍
基于Arduino 开源的 Q900/FT817  显示扩展
根据 817 数据协议 可扩展显示国赫电子Q900 八重州FT817的
1.    频率
2.    工作模式
3.    波段
4.    异频状态
5.    发射状态
6.    S表信息
7.    驻波
8.    参考波长


#### 主要硬件
1.    Arduino UNO/MINI/Pro
2.    2.4寸 TFT (SPFD5408芯片)
3.    JDY-34 蓝牙模块(817无需)
4.    按键(817无需)
5.    其他

#### 安装教程
1.  将TFT SPFD5408(SPFD5408-master.zip)库添加到ArduinoIDE中:
    
![输入图片说明](https://images.gitee.com/uploads/images/2021/0204/174507_2c92084f_5647886.png "在这里输入图片标题")
     
2.  使用Arduino IDE开发工具将项目编译,并上传到Arduon模块
    
2.  数据连接方!
        Q900:蓝牙模块电源连接,RX/TX 分别链接Arduino模块的 10/11 IO接口;
        FT817:根据FT817 的数据连接定义,分别将数据RX/TX连接Arduino 10/11 IO接口;
3.  TFT屏幕 按照标注定义连接Arduino 个接口


#### 开源协议
本项目 遵循 GNU GPL 开源协议;

#### 感谢
stdevPavelmc Pavel Milanes (CO7WT) 所开源的
FT817 Arduino library 
[https://github.com/stdevPavelmc/ft817](https://github.com/stdevPavelmc/ft817)


#### 更新日志
2021.02.04
版本:0.1 测试版
1.    优化代码结构
2.    优化部分数据判断算法

已知问题
1.FT817    数据协议暂未测试
2.Q900     部分协议未全,保持更新中
