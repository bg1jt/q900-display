#include <SoftwareSerial.h>
#include "ft817.h"
#include "display.h"

#define BT_DATA //如果不适用蓝牙数据注释掉
#define VER 0.1//版本
#define S_TIME_OUT 1000 //串口超时时间
#define BT_STATE_PIN 13 //蓝牙状态端口
Display tftDisPlay;//显示模块

SoftwareSerial portInfo(10, 11);//模拟数据接口
FT817 radio;
//时间计数
unsigned long startTime = millis();

 

#ifdef BT_DATA
//硬件连接状态 蓝牙连接使用
int DriveState=0;//0:蓝牙AT模式 1:AT 模式搜索蓝牙 2:蓝牙已配对等待连接 99:已连接但未初始化UI 100:蓝牙连接并已初始化UI 

#else

int DriveState=100;  

#endif


void setup() {
  //Serial.begin(9600, SERIAL_8N2);
  Serial.begin(9600);   
  //portInfo.begin(9600);
  radio.setSerial(portInfo);
  radio.begin(9600);
  tftDisPlay.InIt();
  pinMode(BT_STATE_PIN,INPUT);
  
  inIt();
}

//初始化信息
void inIt() {
  tftDisPlay.WriteText("Q900/FT817 Display",10,10,0xFFFF,2,true);
  tftDisPlay.WriteText("Ver:"+String(VER),10,30,0xFFFF,2,false);
  tftDisPlay.WriteText("BY BG1JT",10,50,0xFFFF,2,false);    
  

  #ifdef BT_DATA  
 
  //检测是否进入AT模式
  //radio.PrintSerialData("AT\r\n");  
  //Serial.println("AT SEND");
  //if (radio.ReadSerialData().indexOf("+OK")>-1)//从蓝牙那里读取数据
   //初始化按键高电位,进入蓝牙设置+配对模式
  delay(1000);
  Serial.println(digitalRead(BT_STATE_PIN));
  if(digitalRead(BT_STATE_PIN)!=LOW )
  {
    Serial.println("AT INIT"); 
    DriveState=0;
    BTSeting();
    DriveState=1;
  }
  else{
    Serial.println("no AT"); 
    //未进入AT模式
     DriveState=2;      
     tftDisPlay.WriteText("Wait For Connect...",40,190,0xFFFF,2,false); 
  }  
  #endif
}

//蓝牙相关设置 (817用不到)
#ifdef BT_DATA  

//蓝牙AT设置
void BTSeting()
{
  tftDisPlay.WriteText("BT Seting,",10,70,0xF800,2,false);
  
  //初始化
  radio.PrintSerialData("AT+DEFAULT\r\n");  
  //delay(400);
  //设置主模式  
  radio.PrintSerialData("AT+BTMODE2\r\n");  
  tftDisPlay.WriteText("SET Master",10,90,0x001F,2,false);
  //打开链接输出
  delay(200);
  radio.PrintSerialData("AT+ENLOG0\r\n");   
  //设置波特率9600
  delay(200);
  radio.PrintSerialData("AT+BAUD4\r\n");      
  //设置波特率无密码
  delay(200);
  tftDisPlay.WriteText("Connect Seting",10,110,0x001F,2,false);
  radio.PrintSerialData("AT+TYPE0\r\n");     
  //设置名称
  //delay(200);
  //radio.PrintSerialData("AT+NAMBQ900-BT_BLE\r\n");  
  //delay(200);
  //radio.PrintSerialData("AT+NAMEQ900-BT\r\n");  
  //delay(200);
  //radio.PrintSerialData("AT+RESET\r\n");  
  tftDisPlay.WriteText("Set Over",10,130,0x001F,2,false);   
  delay(1500);
  startTime-=5000;
  tftDisPlay.WriteText("BT Scanning...",10,10,0x001F,2,true);
  radio.ClreaBtSerial();
}

//搜索蓝牙设备
void SearchBT(){

   //Serial.println("SearchBT"); 
   if(startTime+10000<= millis())
   {
      Serial.println("SearchBT"); 
      radio.PrintSerialData("AT+INQ\r\n");      
      startTime=millis();
   }
   else
   {
      String tmpStr= radio.ReadSerialData();
       Serial.println("SBTRt:"+tmpStr); 
      if(tmpStr=="+SINQ")
      {
        startTime-=5000;
      }
      else if(tmpStr.indexOf("Q900-BT")!=-1)
      {
        radio.PrintSerialData("AT+SINQ\r\n");   
        radio.ReadSerialData();        
        tftDisPlay.WriteText("Connect "+tmpStr,10,40,0x001F,2,false);        
        String mac= tmpStr.substring(7,tmpStr.indexOf(","));
        radio.PrintSerialData("AT+CONA"+mac+"\r\n");
        //ReadSerialData(); 
        delay(200);
        //radio.PrintSerialData("AT+RESET\r\n");                     
        delay(1000);
        DriveState=2;
        radio.ClreaBtSerial();
        return;
      }
   }
}

//蓝牙连接循环判断
void BTConnectDrive(){
  tftDisPlay.WriteText("Wait For Connect...",40,190,0xFFFF,2,false); 
  //检测蓝牙状态电位
  if(digitalRead(BT_STATE_PIN)!=LOW )
  {
    tftDisPlay.WriteText("BT Connect!",60,100,0xFFFF,3,true);     
    DriveState=99;
    delay(1000);
  }
}


#endif


//设备数据
void RadioData(){      
    //高检测评率
    
    //获取频率
    unsigned long freq=radio.getFreqMode()*10;
    tftDisPlay.setFeq(freq);
    
    //发射状态
    bool TxState=radio.chkTX();
    tftDisPlay.setRTDisplay(TxState);  
    /*if(TxState)
    {*/
      // 清空S表
      //tftDisPlay.SetSDisplay(0,0);
      //驻波更新(待测试817协议)
      //tftDisPlay.SetSwrDisplay(radio.Swr);      
    /*}
    else
    {*/
      //清空驻波
      //tftDisPlay.SetSwrDisplay(-1);
      ShowSMeter();
    //}
    
    //低检测频率
    if(startTime+2000<= millis())
    {
      byte _rDate=radio.getMode();
      //Serial.println(radio.Split);
      //模式
      tftDisPlay.setModel(int(_rDate));  
      tftDisPlay.setSplit(radio.Split); 
      startTime=millis();
    }   
}


void ShowSMeter(){
    //S表更新
    int t_index= int(radio.getSMeter())-1;
    tftDisPlay.SetSDisplay(t_index);  
}



void loop() {   
  switch(DriveState)
  {
    case 1:
      SearchBT();//搜索BT
      break;      
    case 99:
      tftDisPlay.UIInit();//UI初始化
      DriveState=100;
    break;
    case 100:  
      RadioData();//处理电台逻辑
    break;
    default:
      BTConnectDrive();//蓝牙链接逻辑
    break;
  }  
}
