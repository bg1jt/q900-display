#include <Arduino.h>
#include <SoftwareSerial.h>

class Display
{
  public:
    Display();
    void InIt();
    void UIInit();
    void setSplit(bool tp);
    void setModel(int m_Index);
    void setRTDisplay(bool tp);
    void setFeq(unsigned long frequency);
    void SetSwrDisplay(int swr);
    void SetSDisplay(int sValue);
    void WriteText(String text,int x,int  y,uint16_t color,int fsize,bool clearAll);

  private:
   
    void setBand(String val);
    //变量缓存
    String MD_TMP="X";//工作模式
    char RT_TMP='X';//接收发射状态
    String FERQ_TMP="";//频率
    int SWR_TMP=0; //驻波
    int S_TMP[3]={0,0,0};//历史S表值
    bool SPL_TMP=true;//异频  
    void setWaveLength(unsigned long frequency);//计算波段
    
    const String FREQ_CMP[4] PROGMEM={"x","xx","xxx","xxxx"};//频率补位
    const String Modle_Arry[13] PROGMEM ={"LSB","USB","C W","CWR","A M","","WFM","","F M","","DIG","","PKT"};//模式数组
    //S表对应数组
    const int Smt_Arry[15][2] PROGMEM={{1,0},{2,0},{3,0},{4,0},{5,0},{6,0},{7,0},{8,0},{9,0},{9,10},{9,20},{9,30},{9,40},{9,50},{9,60}};
};
