#include <Arduino.h>
#include <SoftwareSerial.h>

#include "display.h"

// library SPFD5408
//#include <SPFD5408_Adafruit_GFX.h>    // Core graphics library*/  
/*#include <SPFD5408_TouchScreen.h> */    // Touch library
#include <SPFD5408_Adafruit_TFTLCD.h> // Hardware-specific library

// LCD Pin

#define LCD_CS A3
#define LCD_CD A2
#define LCD_WR A1
#define LCD_RD A0
#define LCD_RESET A4 // Optional : otherwise connect to Arduino's reset pin
// LCD Color
#define BLACK   0x0000
#define BLUE    0x001F
#define RED     0xF800
#define GREEN   0x07E0
#define CYAN    0x07FF
#define MAGENTA 0xF81F
#define YELLOW  0xFFE0
#define WHITE   0xFFFF
#define FEQC    0x0000
#define TOP_BUTTON    10623
#define PRG_LINE    33808




Adafruit_TFTLCD tft(LCD_CS, LCD_CD, LCD_WR, LCD_RD, LCD_RESET);

Display::Display(){
 
}

void Display::InIt(){
  tft.reset();
  tft.begin(0x9341);
  tft.setRotation(3); //屏幕显示方向
  tft.fillScreen(BLACK);
}

//初始化屏幕
void Display::UIInit()
{ 
  tft.fillScreen(BLACK);
  //Serial.println(tft.color565(255,126,103));
  
  //TX状态框
  tft.drawRoundRect(250, 5, 60,25, 2, RED);
  
  //S表
  tft.setTextSize (2);
  tft.setTextColor(PRG_LINE);
  tft.setCursor (15, 85);
  tft.print("S");
  tft.setTextSize (1);
  for(int i=1;i<10;i+=2)
  {
    tft.setCursor (38+(15*i),85);
    tft.print(i);
    tft.drawFastVLine(40+(15*i),95,5,PRG_LINE);
  }
  
  tft.drawRect(40,100,135,2,PRG_LINE);
  tft.drawRect(38,100,2,10,PRG_LINE);//前段竖线
  tft.drawRect(38,110,229,2,PRG_LINE);//底部横线

  //Plus红色部分
  tft.setTextColor(RED);
  for(int i=1;i<4;i++)
  {
    tft.setCursor (170+(30*i),85);
    tft.print("+"+String(i*20));
    tft.drawFastVLine(175+(30*i),95,5,RED);
  }
  tft.setCursor (280,85);
  tft.print("dB");
 
  tft.drawRect(175,100,91,2,RED);

  //SWR
  tft.setTextSize (1);
  tft.setTextColor(64342);
  tft.setCursor (15, 120);
  tft.print("SWR");
  tft.drawRect(40,134,90,2,PRG_LINE);
  tft.drawRect(131,134,135,2,RED);
  tft.drawRect(38,134,2,10,PRG_LINE);
  tft.drawRect(38,144,229,2,PRG_LINE);
   
  /*817 只显示高低驻波*/
  tft.setCursor (80,120);
  tft.setTextColor(40915);
  tft.print("LOW");
  tft.setTextColor(RED);
  tft.setCursor (185,120);
  tft.print("HIGH");
  /*
   * 实际驻波UI初始化
   for(int i=1;i<4;i++)
  {
    tft.setCursor (38+(30*i),120);
    tft.print(i);
    tft.drawFastVLine(40+(30*i),129,5,PRG_LINE);
  }
  

  tft.setTextColor(RED);
  for(int i=4;i<6;i++)
  {
    tft.setCursor (38+(30*i),120);
    tft.print(i);
    tft.drawFastVLine(40+(30*i),129,5,RED);
  }
 


  tft.drawCircle(273,122,3,RED);
  tft.drawCircle(269,122,3,RED);
  tft.drawFastVLine(265,129,5,RED);*/

  //SPLIT
  tft.fillRoundRect(157, 5, 80, 25, 2,TOP_BUTTON);//前景色

  //波长
  tft.setTextSize (1);
  tft.setTextColor(WHITE);
  tft.setCursor (12,160);
  tft.print("Full:");
  tft.setCursor (12,169);
  tft.print("(cm)");
  tft.setCursor (165,160);
  tft.print("1/4:");
  tft.setCursor (165,169);
  tft.print("(cm)");
  
  //UI预加载
  //SetSwrDisplay(-1);
  setSplit('0');
  setModel("00");
  setRTDisplay('0');
  setFeq(8888888888);
  //SetSDisplay(0,0);
}



//异频工作状态
void Display::setSplit(bool tp)
{
  if(SPL_TMP==tp)return;
  
  if (!tp)   
    tft.setTextColor(WHITE);
  else    
    tft.setTextColor(RED);

  
  tft.setTextSize (2);
  tft.setCursor (168, 10);
  tft.print("SPLIT");

  SPL_TMP=tp;
}



//设置工作模式
void Display::setModel(int m_Index) {
  if(MD_TMP==m_Index)return;
  String models=Modle_Arry[m_Index];
  
  tft.fillRoundRect(82, 5, 60, 25,2,TOP_BUTTON);//前景色
  tft.setTextColor(WHITE);
  tft.setCursor (94, 10);
  tft.setTextSize (2);
  tft.print(models);
  MD_TMP=m_Index;
}




//设置接收/发射状态TX/RX
void Display::setRTDisplay(bool tp) {

  if(tp==RT_TMP)return;
  uint16_t C;
  String tip;
  if (tp)
  {
    //tft.fillRect(251, 6, 58, 23, BLACK);//前景色
    tft.setTextColor(RED);
  }
  else {
    //tft.fillRect(251, 6, 58, 23, RED);//前景色
    tft.setTextColor(WHITE);
  }
  
  tft.setCursor (262, 10);  
  tft.setTextSize (2); 
  tft.print("T X");
  RT_TMP=tp;
}



//设置频率信息
void Display::setFeq(unsigned long frequency)
{
  String feqVal=String(frequency);
  int feqLen=8-feqVal.length();  
  int i,LH=40;
  String tmpfeq =feqVal;
  if(feqLen>-1)
  {
    tmpfeq=FREQ_CMP[feqLen]+tmpfeq;
  }
  /*if(feqLen<9)
  {
    for(i=0;i<9-feqLen;i++)
    {
      tmpfeq="x"+tmpfeq;
    }
  }*/
    
  if(tmpfeq==FERQ_TMP)return; 
  
  int  p = -1, point = 315;

  if (feqVal.length() > 6)
  {
    setBand(feqVal.substring(0, feqVal.length() - 6));   
  }
  else
  {  
    setBand("");
  }  

  setWaveLength(frequency);
  tft.setTextColor(WHITE);
  tft.setTextSize (5);
  p = -1; 
 
  for (i = 9; i >= 0; i--)
  {
    char _num=tmpfeq.charAt(i);
    char _num_old=FERQ_TMP.charAt(i);
    
    if(_num!=_num_old || _num=='x')
    {    
        tft.fillRect(point, LH, 30, 40, BLACK);     
    }
    
    if(_num!='x')
    {        
      if(i>=6){
        tft.setTextSize (4);
        tft.setCursor (point, LH+8);
      }
      else{tft.setCursor (point, LH);}
      tft.print(_num);
    } 
       
    point -= 30;
    if (p ==2 && i != 0)
    {
      tft.setTextSize (3);
      tft.setCursor (point+9, LH+14);
      tft.print(".");
      point -= 18;
      p = 0;
      tft.setTextSize (5);
    }
    else
    {
      p++;
    }
  } 
  FERQ_TMP=tmpfeq;
}


//设置波段信息
void Display::setBand(String val)
{
 
  if (val == "")
  { 
    val = "OTH";   
  }
  else {
    int feq = val.toInt();

    if (feq >= 3 && feq <= 30)
    {
      val = "H F";
    }
    else if (feq > 30 && feq <= 300)
    {
      val = "VHF";
    }
    else if (feq > 300 )
    {
      val = "UHF";
    }
  }
  
  tft.setCursor (25, 10);
  tft.setTextSize (2);
  tft.setTextColor(WHITE);  
  tft.fillRoundRect(10, 5, 60, 25,2, TOP_BUTTON);//前景色   
  tft.print(val);  
}


//驻波显示
void Display::SetSwrDisplay(int swr){
  if(SWR_TMP==swr)return;

  //817 高低驻波显示
  if(swr==-1)
  {
     tft.fillRect(40, 137, 225,6,  BLACK);
  }
  else if(swr==0){
    if(swr<SWR_TMP)
    {
      tft.fillRect(130, 137, 145,6,  BLACK);
    }    
    tft.fillRect(40, 137, 90,6,  40915);
  }
  else{
    tft.fillRect(40, 137, 225,6,  RED);
  }
  
  /*实际数值 817不生效
  if(swr>50)
  {
      tft.fillRect(40, 137, 225,6,  RED);
  }
  else if(swr>30)
  {
    if(swr<SWR_TMP)
    {
      tft.fillRect(40+(swr/10.0*30.0), 137, 76+(30*(SWR_TMP-swr)),6,  BLACK);
    }
    else
    {
      tft.fillRect(40, 137, swr/10.0*30.0,6,  RED);
    }
    
  }
  else{
    if(swr<SWR_TMP)
    {
      tft.fillRect(40+(swr/10.0*30.0), 137, 76+(30*(SWR_TMP-swr)),6,  BLACK);
    }
    tft.fillRect(40, 137, swr/10.0*30.0,6,  40915);
  }*/
  SWR_TMP=swr;
}


//S表柱状刷新
void Display::SetSDisplay(int sValue){
  if(S_TMP[0]==sValue)return;
  
  int s1=Smt_Arry[sValue][0];
  int s2=Smt_Arry[sValue][1];
  
  if(S_TMP[1]!=s1)
  {
    //int prgW=int(s1)*15;
    if(S_TMP[1]>s1)
    {       
      tft.fillRect(40+s1*15.0,103,(S_TMP[1]-s1)*15.0,6,  BLACK);//前景色
    }
    else{
      tft.fillRect(40+(S_TMP[1]*15.0), 103,(s1-S_TMP[1])*15.0,6,  17241);//前景色
    }  
  }

  if(S_TMP[2]!=s2)
  {   
    if(S_TMP[2]>s2)
    {       
      tft.fillRect(175+s2/20.0*30.0, 103, (S_TMP[2]-s2)/20.0*30.0,6,  BLACK);//前景色
    }
    else
    {      
      tft.fillRect(175, 103, s2/20.0*30.0,6,  RED);//前景色
    }
  }  
  
  S_TMP[0]=sValue;
  S_TMP[1]=s1;
  S_TMP[2]=s2;
}


//显示文字内容
void Display::WriteText(String text,int x,int  y,uint16_t color,int fsize,bool clearAll)
{
  if(clearAll)
  {
    tft.fillScreen(BLACK);
  }  
  tft.setTextSize (fsize);
  tft.setCursor (x,  y);
  tft.setTextColor(color);
  tft.print(text);
}

//计算并显示波长
void Display::setWaveLength(unsigned long frequency)
{
  float TmpL=30000000000.00/frequency;
  tft.fillRect(48, 160, 110, 20, BLACK);     
  tft.setTextColor(WHITE);
  tft.setCursor (48,160);
  tft.print(TmpL);
  tft.fillRect(195, 160, 110, 20, BLACK);   
  tft.setCursor (195,160);
  tft.print(TmpL/4.00);
  
}
